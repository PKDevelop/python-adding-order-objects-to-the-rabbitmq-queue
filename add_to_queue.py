import pika
import json
# The content of the file can be a response from the API
file = open('/Users/pawelkraus/gitlab/python-adding-order-objects-to-the-rabbitmq-queue/orders.json')
data = json.load(file)


for order in data['results']:
    message = json.dumps(order)
    # Docker RabbitMQ connection
    credentials = pika.PlainCredentials('guest', 'guest')
    parameters = pika.ConnectionParameters('localhost', 60903, '/', credentials)
    connection = pika.BlockingConnection(parameters)

    channel = connection.channel()
    channel.queue_declare(queue='orders', durable=True)
    channel.basic_publish(exchange='', routing_key='orders', body=message)

connection.close()